import React from "react";
import ReactDOM from "react-dom";
import { Panel } from "./panel";

ReactDOM.render(React.createElement(Panel), document.querySelector('.page-wrapper'));