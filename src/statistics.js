import React from 'react';
import { VoivodshipList } from './voivodshipList';

const Statistics = React.createClass({
  render() {
    return this.props.showStatistics ?
      <div>
        <h2>{this.props.category} {this.props.year}</h2>
        <VoivodshipList 
          voivodships={this.props.voivodships}
        /> 
      </div> : 
      <h2>Select category and year</h2>;
  }
});
    
export { Statistics };