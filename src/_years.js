import React from 'react';

const YearsList = React.createClass({
  render() {
    let years = [];
    for (let i = 2000; i <= 2012; i++) {
      years.push(i);
    }
    
    let yearsList = years.map((val, idx) => {
      return <option key={idx} value={val}> {val} </option>
    });
    
    return <select onChange={this.props.updateInput} id="year">
      { yearsList }
    </select>
  }
})

export { YearsList };