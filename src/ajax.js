export const ajax = {
	xmlhttp: new XMLHttpRequest(),
	get (src, callback) {  
      this.xmlhttp.onreadystatechange = function() {
          if(this.readyState === 4 && this.status === 200) {
              callback(this.responseText);
          }
      };
      this.xmlhttp.open("GET", src, true);
      this.xmlhttp.send();
	}
};