import React from "react";
import { YearsList } from './_years'
import { CategoriesList } from './_categoriesList'

const Options = React.createClass({
  handleClick(ev) { 
    this.props.getData() 
  },
  updateInput(ev) {
    let value = ev.target.value;
    let input = ev.target.id;
    this.props.updateInput(input, value);
  },
  render() {
    return <div className="options-container"> 
      <CategoriesList updateInput={this.updateInput} />
      <YearsList updateInput={this.updateInput} />
      <button onClick={this.handleClick} className="btn btn-results" type="button">Show results</button>
    </div>
  }
});

export { Options };