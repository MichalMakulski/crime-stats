import React from "react";
import { Header } from "./header";
import { Options } from "./data_options";
import { Statistics } from "./statistics";
import { ajax } from "./ajax";

const Panel = React.createClass({
  getInitialState() {
    return {
      category: 'ogol',
      year: '2000',
      cache: {}
    }
  },
  getData() {
    let category = this.state.category;
    let year = this.state.year;
    if (this.state.cache[category] && this.state.cache[category][year]) {
      console.log('Data is cached...');
    }else {
      ajax.get(`/data/${category}.json`, this.processData);
    }
  },
  processData(data) {
    let currentData = JSON.parse(data)[this.state.year];
    let dataArr = [];
    for (let key in currentData) {
      dataArr.push(currentData[key]);
    }
    this.setState({
      cache: {
        [this.state.category]: {
          [this.state.year]: data
        }
      },
      voivodships: dataArr,
      showStatistics: true
    });
  },
  updateInput(input, value) {
    this.setState({
      [input]: value,
      showStatistics: false
    });
  },
  render() {
    return <div className="controls-container">
      <Header />
      <Options 
        getData={this.getData}
        updateInput={this.updateInput}
      />
      <Statistics
        category={this.state.category}
        year={this.state.year}
        showStatistics={this.state.showStatistics} 
        voivodships={this.state.voivodships}  
      />
    </div>
  }  
});

export { Panel };