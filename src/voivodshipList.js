import React from 'react';

const VoivodshipList = React.createClass({
  render() {
    let voivodships = this.props.voivodships.map((voivodship, idx) => {
      return <tr key={idx} className="voivodship"> 
        <td>{voivodship.name}</td>
        <td>{voivodship.wsz}</td>
        <td>{voivodship.stw}</td>
        <td>{voivodship.wyk}</td>
        <td>{voivodship.pod}</td>
      </tr>
    });
    
    return <table className="statisticsContainer">
      <tbody>
        <tr>
          <th>Voivodship</th>
          <th>Invesigations</th>
          <th>Crimes</th>
          <th>Clear rate (%)</th>
          <th>Suspects</th>
        </tr>
        { voivodships }
      </tbody>
    </table>
  }
});

export { VoivodshipList }