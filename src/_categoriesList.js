import React from 'react';
let categories = [
  {
    name: 'Global',
    nameShort: 'ogol'
  },
  {
    name: 'Crimanal (not economic)',
    nameShort: 'krym'
  },
  {
    name: 'Drug related',
    nameShort: 'nark'
  },
  {
    name: 'Car theft',
    nameShort: 'samo'
  },
  {
    name: 'Breaking and entering',
    nameShort: 'wlam'
  },
  {
    name: 'Economic',
    nameShort: 'gosp'
  },
  {
    name: 'Rape',
    nameShort: 'zgwa'
  },
  {
    name: 'Theft',
    nameShort: 'cudz'
  }
]

const CategoriesList = React.createClass({
  render() {
    
    let categoriesList = categories.map((category, idx) => {
      return <option key={idx} value={category.nameShort}> {category.name} </option>
    });
    
    return <select onChange={this.props.updateInput} id="category">
      { categoriesList }
    </select>
  }
})

export { CategoriesList };