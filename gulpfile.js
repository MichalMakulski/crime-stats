var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var babelify = require('babelify');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var envify = require('envify/custom');

var dependencies = [ 'react', 'react-dom' ];

gulp.task('scripts', function () {
    bundleApp(false);
});

gulp.task('deploy', function (){
	bundleApp(true);
});

gulp.task('watch', function () {
	gulp.watch(['./src/*.js'], ['scripts']);
});

gulp.task('default', ['scripts', 'watch', 'serve', 'sass', 'sass:watch']);

function bundleApp(isProduction) {
	var appBundler = browserify({
      entries: './src/app.js',
    	debug: true,
        cache: {},
        packageCache: {},
        fullPaths: true 
      });

  	appBundler
	  	.transform(babelify.configure({
          presets: ["es2015", "react"]
        }))
      //.transform(envify({global: true, _: 'purge', NODE_ENV: 'production'}), {global: true})
	    .bundle()
	    .on('error', gutil.log)
	    .pipe(source('main.js'))
//      .pipe(buffer())
//      .pipe(uglify())
	    .pipe(gulp.dest('./'));
}

gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("./scss/**/*.scss", ['sass']);
    gulp.watch("./*.html").on('change', browserSync.reload);
    gulp.watch("./*.js").on('change', browserSync.reload);
});

gulp.task('sass', function() {
    return gulp.src("./scss/**/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("./css"))
        .pipe(browserSync.stream());
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./scss/**/*.scss', ['sass']);
});